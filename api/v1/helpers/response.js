const _ = require('lodash');

const response = {

  code: (error) => {
    let es = {
      e400: ['include email'],
      e401: ['include authorization', 'invalid token', 'include secret', 'invalid signature', 'invalid password', 'invalid currentPassword', 'include password', 'include currentPassword'],
      e402: ['upgrade plan'],
      e403: [],
      e404: ['doesn\'t exists'],
      e405: [],
      e406: ['include role'],
      e407: [],
      e408: [],
      e409: ['duplicate key']
    }
    let r = _.findKey(es, e => {return _.find(e, s => {return _.includes(error.text, s);});}); console.log('ERROR:', Object.assign({}, error, {key: r}));
    return r ? _.replace(r, 'e', '') : 500;
  }

};

module.exports = response;