const password = require('bcryptjs');

const bcrypt = {
  hash: async (str) => {
    return await password.hash(str, 13);
  },

  compare: async (raw, hash) => {
    return await password.compare(raw, hash);
  },

  noDot: async (str) => {
    /**
     * @description syntax of all escape characters.
     * . -> zdon
     * & -> zatw
     * $ -> zdth
     */
    return str.replace(/\./gm, 'zdon').replace(/\&/gm, 'zatw').replace(/\$/gm, 'zdth');
  },

  dot: async (str) => {
    return str.replace(/(zdon)/gm, '.').replace(/(zatw)/gm, '&').replace(/(zdth)/gm, '$');
  }
};

module.exports = bcrypt;