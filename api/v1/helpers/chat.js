const _ = require('../models')._;
// const Policies = require('../policies');
const ao = require('../../../environment/').ao;

/**
 * @description DECODES Cookie From String To Object.
 */
const chat = {

  auth: async (Socket, io, param) => {return Object.assign({ website: Socket.handshake.headers.origin }, param.auth);},
  
  filterParams: async (Socket, io, param) => {
    /**
     * @description prevent API domains without apiKey
     */
    if(!param.data.api && !ao.includes(Socket.handshake.headers.origin)) {
      io.to(Socket.id).emit('error', { error: { type: 'error', text: 'include apiKey in data' } }); return false;
    }

    // let r = await Policies.isSecure(Object.assign({ website: Socket.handshake.headers.origin }, param.auth));
    // if(r.error) { io.to(Socket.id).emit('error', r); return false; }
    /**
     * @description maintain user security with less data
     */
    r.data = _.pick(r.data, ['avatar', 'email', 'fullName']);
    /**
     * @description loggedIn response for other API domains.
     */
    if(!ao.includes(Socket.handshake.headers.origin)) {
      r.data = {
        avatar: `${process.env.DEVELOPMENT ? 'http://localhost:5000' : 'https://ps.herokuapp.com'}/image/avatar/round-person-black-18/2x/round_person_black_18dp.png`,
        email: param.data.email || Socket.id,
        fullName: param.data.fullName || 'Visitor'
      };
    }
    return r;
  }

};

module.exports = chat;