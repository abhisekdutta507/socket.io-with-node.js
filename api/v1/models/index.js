
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');
const bycript   = require('../helpers/bcrypt');

const Models = {

  data: {
    selector: {
      chat: ['api', 'createdAt', 'createdBy', 'messages', 'updatedAt', 'users', '_id'],
      api: ['user', 'website', 'createdAt']
    }
  },

  bycript: bycript,

  _: _,
};

module.exports = Models;