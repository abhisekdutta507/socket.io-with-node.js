const Models = require('../models/').objects;
const _ = require('../models/')._;
const jwt = require('../helpers/jwt');
// const cookie = require('../helpers/cookie');
const response = require('../helpers/response');
const ao = require('../../../environment/').ao;

const policies = {
  track: async (req, res, next) => {
    next();
  },

  isLoggedIn: async (req, res, next) => {
    if(!req.headers.authorization) { return res.status(response.code({text: 'please include authorization in header'})).set('Content-Type', 'application/json').send({ error: { type: 'error', text: 'please include authorization in header' } }); }
    /**
     * @description Decode and Authenticate JWT token.
     */
    const token = await jwt.decode(req.headers.authorization);
    if(token.error) {
      return res.status(response.code({text: token.error})).set('Content-Type', 'application/json').send({ type: 'error', text: token.error });
    }

    /**
     * @description If the user has a role then his email
     * must match the authorization token.
     */
    if(token.role && req.params.email) {
      if(req.params.email !== token.email) { return res.status(response.code({text: 'invalid token'})).set('Content-Type', 'application/json').send({ type: 'error', text: 'invalid token' }); }
    }

    const u = await Models.user.findOne(
      _.pick(token, ['email', 'jwtValidatedAt', 'role'])
    );
    if(u.error) { return res.status(response.code(u.error)).set('Content-Type', 'application/json').send(u.error); }

    req.auth = { user: u };

    next();
  },

  isAdmin: async (req, res, next) => {
    if(!req.headers.authorization) { return res.status(response.code({text: 'please include authorization in header'})).set('Content-Type', 'application/json').send({ error: { type: 'error', text: 'please include authorization in header' } }); }
    /**
     * @description Decode and Authenticate JWT token.
     */
    const token = await jwt.decode(req.headers.authorization);
    if(token.error) {
      return res.status(response.code({text: token.error})).set('Content-Type', 'application/json').send({ type: 'error', text: token.error });
    }

    /**
     * @description The user must has a role 0.
     */
    if(token.role) { return res.status(response.code({text: 'invalid token'})).set('Content-Type', 'application/json').send({ type: 'error', text: 'invalid token' }); }

    const u = await Models.user.findOne(
      _.pick(token, ['email', 'jwtValidatedAt', 'role'])
    );
    if(u.error) { return res.status(response.code(u.error)).set('Content-Type', 'application/json').send(u.error); }

    req.auth = { user: u };

    next();
  },

  isSecure: async (auth) => {
    /**
     * @description auth must contain authorization or secret key objects.
     */
    if(!auth.website) { return { error: { type: 'error', text: 'please include secret in auth' } }; }
    /**
     * @description by pass the security when domain in zazuconnection.
     */
    if(ao.includes(auth.website)) {
      if(!auth.authorization) { return { error: { type: 'error', text: 'please include authorization in auth' } }; }

      let token = await jwt.decode(auth.authorization);
      if(token.error) { return token; }

      return await Models.user.findOne(
        _.pick(token, ['email', 'jwtValidatedAt', 'role'])
      );
    } else {
      if(!auth.secret) { return { error: { type: 'error', text: 'please include secret in auth' } }; }

      let a = await Models.api.findOne( _.pick(auth, ['website', 'secret']) );
      if(a.error) { return a; }
      /**
       * @description after API validation initializes the user object.
       */
      return {
        message: { type: 'success' },
        data: a.data.user
      };
    }
  },

};

module.exports = policies;