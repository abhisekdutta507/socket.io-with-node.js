const _ = require('../models')._;
const ao = require('../../../environment/').ao;
let io;

const ChatController = {

  data: {
    users: {},
    initCRId: 'static_croom_'
  },

  connect: async (server) => {
    io = require('socket.io')(server, {
      origins: ao
    });
    /**
     * @description EventListeners should stay inside the connect().
     */
    io.on('connection', async (Socket) => {
      /**
       * @description bind the event listeners here.
       */
      Socket.on('login', ChatController.login);
      Socket.on('render-video', ChatController.renderVideo);
    });
  },

  login: async function (param) {
    if (!param.username) {
      io.to(this.id).emit('error', { messages: 'username is missing in data', data: param });
      return;
    }

    const { users, initCRId } = ChatController.data;
    if (!users[param.username]) {
      users[param.username] = {
        ids: [],
        room: `${initCRId}${param.username}`
      };
    }

    users[param.username].ids.push(this.id);

    // io.to(this.id).emit('loggedIn', { ...param });
    this.join(users[param.username].room, async () => {
      io.to(users[param.username].room).emit('loggedIn', { ...param });
    });
  },

  renderVideo: async function (param) {
    if (!param.username) {
      io.to(this.id).emit('error', { messages: 'username is missing in data', data: param });
      return;
    }

    const { users } = ChatController.data;
    if (!users[param.username]) {
      io.to(this.id).emit('error', { messages: 'username is valid', data: param });
      return;
    }

    setTimeout(() => {
      io.to(users[param.username].room).emit('rendered', { ...param });
    }, 5000);
  }

};

module.exports = ChatController;