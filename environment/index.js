const env = {
  Mlab: {
    host: '',
    port: 123456,
    database: '',
    username: '',
    password: ''
  },
  JWT: {
    key: '45gH8pt'
  },
  cloudinary: {
    cloud_name: 'imagebag',
    api_key: '', 
    api_secret: ''
  },
  Google: {
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
      user: 'abhisek.dutta.507@gmail.com',
      pass: ''
    }
  },

  /**
   * @description Allowed Origins are listed here.
   */
  ao: [
    'http://localhost'
    , 'http://localhost:3000'
    , 'http://localhost:3001'
    , 'http://localhost:3002'
    , 'http://localhost:3003'
    , 'http://localhost:3004'
    , 'http://localhost:3005'
    , 'http://localhost:4000'
    , 'http://localhost:4001'
    , 'http://localhost:4002'
    , 'http://localhost:4003'
    // , 'http://localhost:4004'
    , 'http://localhost:4005'
    , 'http://localhost:4040'
    , 'http://localhost:4100'
    , 'http://localhost:4200'
    , 'http://localhost:8080'
    , 'http://localhost:8081'
  ]
};

module.exports = env;