const express = require('express');
const api = express.Router();
const Test = require('../api/v1/controller/Test');

/**
 * @description version 1 API routes
 */
const routes = () => {
  api.get('/test', Test.findAll);

  return api;
};

module.exports = routes();